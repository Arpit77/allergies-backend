"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.validateBody = void 0;
const validateBody = (schema) => {
    return (req, res, next) => {
        const { error } = schema.validate(req.body, {});
        error ? next({ msg: error.details[0].message }) : next();
    };
};
exports.validateBody = validateBody;
