"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.errorHandler = void 0;
const logger_1 = __importDefault(require("../utils/logger"));
function errorHandler(error, req, res, next) {
    var _a;
    logger_1.default.error((_a = error.msg) !== null && _a !== void 0 ? _a : error);
    res.status(error.statusCode || 400).json({ error: error.msg });
}
exports.errorHandler = errorHandler;
