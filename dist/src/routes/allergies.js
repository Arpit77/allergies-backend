"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const validate_1 = require("../middlewares/validate");
const authenticate_1 = require("../middlewares/authenticate");
const allergyController = __importStar(require("../controller/allergies"));
const checkUserExists_1 = require("../middlewares/checkUserExists");
const schema_1 = require("../utils/schema");
const router = (0, express_1.Router)();
router.use(authenticate_1.authenticate);
router.use(checkUserExists_1.checkUserExists);
router
    .route("/")
    .get(allergyController.getAllergies)
    .post((0, validate_1.validateBody)(schema_1.allergySchema), allergyController.createAllergy);
router
    .route("/:id")
    .get(allergyController.getAllergy)
    .put((0, validate_1.validateBody)(schema_1.updateAllergySchema), allergyController.updateAllergy)
    .delete(allergyController.deleteAllergy);
router.put("/mark-as-high-risk/:id", (0, validate_1.validateBody)(schema_1.updateAllergySchema), allergyController.markAsHighRisk);
router.post("/search", allergyController.searchAllergy);
router.post("/upload-image", allergyController.imageUpload);
router.get("/get-comments/:id", allergyController.getComments);
router.put("/add-comment/:id", allergyController.addComment);
router.put("/delete-comment/:id", allergyController.deleteComment);
exports.default = router;
