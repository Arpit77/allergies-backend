"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.getUsers = exports.getUser = exports.refresh = exports.register = exports.login = void 0;
const authService = __importStar(require("../services/auth"));
const createTokens_1 = require("./../utils/createTokens");
const verifyRefreshToken_1 = require("../utils/verifyRefreshToken");
const login = (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const data = yield authService.login(req.body);
        res.status(200).json(data);
    }
    catch (e) {
        next(e);
    }
});
exports.login = login;
const register = (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const data = yield authService.register(req.body);
        res.status(201).json(data);
    }
    catch (e) {
        next(e);
    }
});
exports.register = register;
const refresh = (req, res, next) => {
    try {
        const { id, refreshToken } = req.body;
        const isValid = (0, verifyRefreshToken_1.verifyRefresh)(id, refreshToken);
        if (!isValid) {
            return next({ msg: "invalid token" });
        }
        const newAccessToken = (0, createTokens_1.createAccessToken)({ userId: id });
        res.status(200).json({
            data: { newAccessToken },
            message: "new access token generated...",
        });
    }
    catch (e) {
        next(e);
    }
};
exports.refresh = refresh;
const getUser = (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const user = yield authService.getUser(+req.params.id);
        res.status(200).json(user);
    }
    catch (e) {
        next(e);
    }
});
exports.getUser = getUser;
const getUsers = (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const allUsers = yield authService.getAllUsers();
        res.status(200).json(allUsers);
    }
    catch (e) {
        next(e);
    }
});
exports.getUsers = getUsers;
