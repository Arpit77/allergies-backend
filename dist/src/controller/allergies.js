"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.getComments = exports.deleteComment = exports.addComment = exports.searchAllergy = exports.imageUpload = exports.markAsHighRisk = exports.deleteAllergy = exports.updateAllergy = exports.createAllergy = exports.getAllergy = exports.getAllergies = void 0;
const allergyService = __importStar(require("../services/allergies"));
const uploadImage_1 = require("../utils/uploadImage");
const getAllergies = (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const pageSize = +req.query.pagesize || 10;
        const currentPage = +req.query.page || 1;
        const allAllergies = yield allergyService.getAll({
            pageSize,
            currentPage,
        });
        res.status(200).json(allAllergies);
    }
    catch (e) {
        next(e);
    }
});
exports.getAllergies = getAllergies;
const getAllergy = (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const allergy = yield allergyService.getOne(+req.params.id);
        res.status(200).json(allergy);
    }
    catch (e) {
        next(e);
    }
});
exports.getAllergy = getAllergy;
const createAllergy = (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const userId = req.user.id;
        const newAllergy = yield allergyService.create(req.body, userId);
        res.status(201).json(newAllergy);
    }
    catch (e) {
        next(e);
    }
});
exports.createAllergy = createAllergy;
const updateAllergy = (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const updatedAllergy = yield allergyService.update(+req.params.id, req.body);
        res.status(200).json(updatedAllergy);
    }
    catch (e) {
        next(e);
    }
});
exports.updateAllergy = updateAllergy;
const deleteAllergy = (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        yield allergyService.remove(+req.params.id);
        res.status(204).json(null);
    }
    catch (e) {
        next(e);
    }
});
exports.deleteAllergy = deleteAllergy;
const markAsHighRisk = (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const updatedAllergy = yield allergyService.markAsHighRisk(req.body, +req.params.id);
        res.status(200).json({
            data: updatedAllergy,
        });
    }
    catch (e) {
        next(e);
    }
});
exports.markAsHighRisk = markAsHighRisk;
const imageUpload = (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        let result;
        if (req.files && req.files.image) {
            const file = req.files.image;
            result = yield (0, uploadImage_1.uploadFromBuffer)(file.data);
        }
        else {
            result = yield (0, uploadImage_1.uploadImage)(req.body.image);
        }
        res.status(200).json(result);
    }
    catch (e) {
        next({ msg: e.message });
    }
});
exports.imageUpload = imageUpload;
const searchAllergy = (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const { query } = req.body;
        let searchedAllergy;
        if (query) {
            searchedAllergy = yield allergyService.search(query);
        }
        else {
            searchedAllergy = yield allergyService.getAll({
                pageSize: 3,
                currentPage: 1,
            });
        }
        res.status(200).json(searchedAllergy);
    }
    catch (e) {
        next(e);
    }
});
exports.searchAllergy = searchAllergy;
const addComment = (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const commentData = {
            comment: req.body.comment,
            addedBy: {
                name: req.user.name,
                id: req.user.id,
            },
            createdAt: new Date(Date.now()),
        };
        const updatedAllergy = yield allergyService.addComment(+req.params.id, commentData);
        res.status(200).json(updatedAllergy);
    }
    catch (e) {
        next(e);
    }
});
exports.addComment = addComment;
const deleteComment = (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const { comment, createdAt } = req.body;
        const commentData = {
            comment,
            createdAt,
            addedBy: {
                name: req.user.name,
                id: req.user.id,
            },
        };
        const updatedAllergy = yield allergyService.deleteComment(+req.params.id, commentData);
        res.status(200).json(updatedAllergy);
    }
    catch (e) {
        next(e);
    }
});
exports.deleteComment = deleteComment;
const getComments = (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const queriedComments = yield allergyService.getComments(+req.params.id);
        res.status(200).json(queriedComments);
    }
    catch (e) {
        next(e);
    }
});
exports.getComments = getComments;
