"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const winston_1 = require("winston");
const logFormat = winston_1.format.printf(({ level, message, timestamp, stack }) => {
    return `[${timestamp}] [${level}] : ${stack || message}`;
});
const logger = (0, winston_1.createLogger)({
    format: winston_1.format.combine(winston_1.format.colorize(), winston_1.format.timestamp({ format: 'YYYY-MM-DD HH:mm:ss' }), logFormat),
    transports: [new winston_1.transports.Console()],
});
exports.default = logger;
