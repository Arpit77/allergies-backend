"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.handleError = void 0;
const client_1 = require("@prisma/client");
const { PrismaClientKnownRequestError, PrismaClientInitializationError, PrismaClientValidationError, } = client_1.Prisma;
function handleError(e) {
    if (e instanceof PrismaClientInitializationError) {
        return ({
            msg: "error connecting to the database.",
        });
    }
    else if (e instanceof PrismaClientKnownRequestError) {
        switch (e.code) {
            case "P2002":
                return ({
                    msg: "This email is already registered.",
                });
            default:
                return (e);
        }
    }
    else if (e instanceof PrismaClientValidationError) {
        return ({
            msg: "db validation failed.",
        });
    }
    else {
        return (e);
    }
}
exports.handleError = handleError;
