"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.verifyRefresh = void 0;
const jsonwebtoken_1 = __importDefault(require("jsonwebtoken"));
const JWT_REFRESH = process.env.JWT_REFRESH;
function verifyRefresh(id, token) {
    let isValidToken;
    jsonwebtoken_1.default.verify(token, JWT_REFRESH, (_, decoded) => {
        if (decoded.userId === id) {
            isValidToken = true;
        }
        else {
            isValidToken = false;
        }
    });
    return isValidToken;
}
exports.verifyRefresh = verifyRefresh;
