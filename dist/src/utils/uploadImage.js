"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.uploadFromBuffer = exports.uploadImage = void 0;
const cloudinary = require("cloudinary");
let streamifier = require("streamifier");
cloudinary.config({
    cloud_name: process.env.cloud_name,
    api_key: process.env.api_key,
    api_secret: process.env.api_secret,
});
const uploadImage = (image) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const result = yield cloudinary.v2.uploader.upload(image, {
            transformation: [
                { gravity: "face", height: 300, width: 200, crop: "crop" },
                { radius: "max" },
                { width: 150, crop: "scale" },
            ],
        });
        return result.secure_url;
    }
    catch (e) {
        return e;
    }
});
exports.uploadImage = uploadImage;
const uploadFromBuffer = (buffer) => {
    return new Promise((resolve, reject) => {
        const uploadStream = cloudinary.v2.uploader.upload_stream({
            transformation: [{ gravity: "face", crop: "crop" }],
        }, (error, result) => {
            if (result) {
                resolve(result.secure_url);
            }
            else {
                reject(error);
            }
        });
        streamifier.createReadStream(buffer).pipe(uploadStream);
    });
};
exports.uploadFromBuffer = uploadFromBuffer;
