"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.decryptPassword = exports.passwordHash = void 0;
const crypto_1 = __importDefault(require("crypto"));
const algorithm = "aes-192-cbc";
const key = crypto_1.default.scryptSync("secret", "salt", 24);
const passwordHash = (password) => {
    return new Promise((resolve, reject) => {
        try {
            const iv = crypto_1.default.randomBytes(16);
            const cipher = crypto_1.default.createCipheriv(algorithm, key, iv);
            const encrypted = cipher.update(password, "utf8", "hex");
            const data = [
                encrypted + cipher.final("hex"),
                Buffer.from(iv).toString("hex"),
            ].join("|");
            resolve(data);
        }
        catch (e) {
            reject(e);
        }
    });
};
exports.passwordHash = passwordHash;
const decryptPassword = (password) => {
    return new Promise((resolve, reject) => {
        try {
            const [encrypted, iv] = password.split("|");
            if (!iv)
                throw new Error("IV not found");
            const decipher = crypto_1.default.createDecipheriv(algorithm, key, Buffer.from(iv, "hex"));
            resolve(decipher.update(encrypted, "hex", "utf8") + decipher.final("utf8"));
        }
        catch (e) {
            reject(e);
        }
    });
};
exports.decryptPassword = decryptPassword;
