"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.updateAllergySchema = exports.allergySchema = exports.userSchema = void 0;
const joi_1 = __importDefault(require("joi"));
exports.userSchema = joi_1.default.object({
    name: joi_1.default.string().required(),
    email: joi_1.default.string().required().email(),
    password: joi_1.default.string().required().min(5),
    role: joi_1.default.string()
});
exports.allergySchema = joi_1.default.object({
    name: joi_1.default.string().required().max(25),
    severity: joi_1.default.string().required().max(20),
    image: joi_1.default.string(),
    highRisk: joi_1.default.bool(),
    symptoms: joi_1.default.array().required().items(joi_1.default.string()),
    comment: joi_1.default.string(),
});
exports.updateAllergySchema = joi_1.default.object({
    name: joi_1.default.string().max(25),
    severity: joi_1.default.string().max(20),
    image: joi_1.default.string(),
    highRisk: joi_1.default.bool(),
    symptoms: joi_1.default.array().items(joi_1.default.string()),
    comment: joi_1.default.string(),
});
