"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.getAllUsers = exports.getUser = exports.register = exports.login = void 0;
const client_1 = __importDefault(require("../db/client"));
const excludeField_1 = require("../utils/excludeField");
const handleError_1 = require("../utils/handleError");
const passwordHash_1 = require("../utils/passwordHash");
const createTokens_1 = require("../utils/createTokens");
const login = (body) => {
    return new Promise((resolve, reject) => __awaiter(void 0, void 0, void 0, function* () {
        try {
            const { email, password } = body;
            if (!email || !password) {
                return reject({
                    msg: "please enter email and password.",
                });
            }
            const user = yield client_1.default.user.findUnique({ where: { email } });
            if (!user) {
                return reject({ msg: "invalid email or password" });
            }
            if (user) {
                const decryptedPassword = yield (0, passwordHash_1.decryptPassword)(user.password);
                if (decryptedPassword !== password) {
                    return reject({ msg: "invalid email or password" });
                }
            }
            const accessToken = (0, createTokens_1.createAccessToken)({ userId: user.id });
            const refreshToken = (0, createTokens_1.createRefreshToken)({ userId: user.id });
            const userWithoutPassword = (0, excludeField_1.exclude)(user, "password");
            resolve({
                data: { accessToken, refreshToken, user: userWithoutPassword },
                message: "Logged in successfully...",
            });
        }
        catch (e) {
            reject((0, handleError_1.handleError)(e));
        }
    }));
};
exports.login = login;
const register = (body) => {
    return new Promise((resolve, reject) => __awaiter(void 0, void 0, void 0, function* () {
        try {
            const { name, email, password, role } = body;
            const allowedRoles = ["user", "admin"];
            if (role && !allowedRoles.includes(role)) {
                return reject({
                    msg: "sorry, that role is not allowed. please contact the administrator.",
                });
            }
            const hashedPassword = yield (0, passwordHash_1.passwordHash)(password);
            const user = yield client_1.default.user.create({
                data: {
                    name,
                    email,
                    password: hashedPassword,
                    role,
                },
            });
            const userWithoutPassword = (0, excludeField_1.exclude)(user, "password");
            resolve({
                data: { user: userWithoutPassword },
                message: "sign up completed successfully...",
            });
        }
        catch (e) {
            reject((0, handleError_1.handleError)(e));
        }
    }));
};
exports.register = register;
function getUser(userId) {
    return new Promise((resolve, reject) => {
        client_1.default.user
            .findFirst({
            where: {
                id: userId,
            },
            include: {
                allergies: true
            }
        })
            .then((user) => {
            if (!user) {
                return reject({ msg: "no such user exists." });
            }
            const userWithoutPassword = (0, excludeField_1.exclude)(user, "password");
            resolve(userWithoutPassword);
        })
            .catch((err) => reject((0, handleError_1.handleError)(err)));
    });
}
exports.getUser = getUser;
function getAllUsers() {
    return new Promise((resolve, reject) => {
        client_1.default.user
            .findMany()
            .then((users) => {
            if (users.length) {
                const usersWithoutPassword = users.map((user) => (0, excludeField_1.exclude)(user, "password"));
                resolve(usersWithoutPassword);
            }
            else {
                resolve(users);
            }
        })
            .catch((err) => reject((0, handleError_1.handleError)(err)));
    });
}
exports.getAllUsers = getAllUsers;
