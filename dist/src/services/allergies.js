"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.getComments = exports.deleteComment = exports.addComment = exports.search = exports.markAsHighRisk = exports.remove = exports.update = exports.create = exports.getOne = exports.getAll = void 0;
const client_1 = __importDefault(require("../db/client"));
const handleError_1 = require("../utils/handleError");
const getAll = ({ pageSize, currentPage }) => {
    return new Promise((resolve, reject) => {
        client_1.default.allergies
            .findMany({
            skip: pageSize * (currentPage - 1),
            take: pageSize,
            orderBy: [
                {
                    highRisk: "desc",
                },
                {
                    name: "asc",
                },
            ],
        })
            .then((data) => resolve(data))
            .catch((err) => reject((0, handleError_1.handleError)(err)));
    });
};
exports.getAll = getAll;
const getOne = (id) => {
    return new Promise((resolve, reject) => {
        client_1.default.allergies
            .findUnique({ where: { id } })
            .then((allergies) => __awaiter(void 0, void 0, void 0, function* () {
            if (!allergies) {
                return reject({
                    msg: "allergy not found.",
                });
            }
            resolve(Object.assign(Object.assign({}, allergies), { comments: allergies.comments.reverse().slice(0, 5).reverse() }));
        }))
            .catch((err) => reject((0, handleError_1.handleError)(err)));
    });
};
exports.getOne = getOne;
const create = (body, userId) => __awaiter(void 0, void 0, void 0, function* () {
    return new Promise((resolve, reject) => __awaiter(void 0, void 0, void 0, function* () {
        try {
            const allergy = yield client_1.default.allergies.create({
                data: Object.assign(Object.assign({}, body), { userId }),
            });
            resolve(allergy);
        }
        catch (e) {
            reject((0, handleError_1.handleError)(e));
        }
    }));
});
exports.create = create;
const update = (id, body) => __awaiter(void 0, void 0, void 0, function* () {
    return new Promise((resolve, reject) => __awaiter(void 0, void 0, void 0, function* () {
        try {
            const allergy = yield (0, exports.getOne)(id);
            if (!allergy) {
                return reject({ msg: "no such data exists." });
            }
            const updatedAllergy = yield client_1.default.allergies.update({
                where: {
                    id: allergy.id,
                },
                data: body,
            });
            resolve(updatedAllergy);
        }
        catch (e) {
            reject((0, handleError_1.handleError)(e));
        }
    }));
});
exports.update = update;
const remove = (id) => {
    return new Promise((resolve, reject) => __awaiter(void 0, void 0, void 0, function* () {
        try {
            const allergy = yield (0, exports.getOne)(id);
            if (!allergy) {
                return reject({ msg: "no such data exists." });
            }
            yield client_1.default.allergies.delete({
                where: {
                    id: allergy.id,
                },
            });
            resolve(true);
        }
        catch (e) {
            reject((0, handleError_1.handleError)(e));
        }
    }));
};
exports.remove = remove;
const markAsHighRisk = (body, allergyId) => {
    return new Promise((resolve, reject) => {
        client_1.default.allergies
            .updateMany({
            where: {
                id: allergyId,
            },
            data: body,
        })
            .then((data) => resolve(data))
            .catch((err) => reject((0, handleError_1.handleError)(err)));
    });
};
exports.markAsHighRisk = markAsHighRisk;
const search = (query) => {
    return new Promise((resolve, reject) => {
        client_1.default.allergies
            .findMany({
            where: {
                name: {
                    contains: query,
                    mode: "insensitive",
                },
            },
        })
            .then((data) => resolve(data))
            .catch((err) => reject((0, handleError_1.handleError)(err)));
    });
};
exports.search = search;
const addComment = (id, commentData) => {
    return new Promise((resolve, reject) => __awaiter(void 0, void 0, void 0, function* () {
        try {
            const allergy = yield (0, exports.getOne)(id);
            if (!allergy) {
                return reject({ msg: "no such data exists." });
            }
            const updatedAllergy = yield client_1.default.allergies.update({
                where: {
                    id: allergy.id,
                },
                data: {
                    comments: {
                        push: commentData,
                    },
                },
            });
            resolve(updatedAllergy);
        }
        catch (e) {
            reject((0, handleError_1.handleError)(e));
        }
    }));
};
exports.addComment = addComment;
const deleteComment = (id, commentData) => {
    return new Promise((resolve, reject) => __awaiter(void 0, void 0, void 0, function* () {
        try {
            const allergy = yield (0, exports.getOne)(id);
            if (!allergy) {
                return reject({ msg: "no such data exists." });
            }
            const allComments = yield (0, exports.getComments)(allergy.id);
            const { comment, addedBy, createdAt } = commentData;
            const updatedAllergy = yield client_1.default.allergies.update({
                where: {
                    id: allergy.id,
                },
                data: {
                    comments: {
                        set: allComments.comments.filter((cmt) => cmt.comment !== comment ||
                            cmt.addedBy.id !== addedBy.id ||
                            cmt.createdAt !== createdAt),
                    },
                },
            });
            resolve(updatedAllergy);
        }
        catch (e) {
            reject((0, handleError_1.handleError)(e));
        }
    }));
};
exports.deleteComment = deleteComment;
const getComments = (id) => {
    return new Promise((resolve, reject) => __awaiter(void 0, void 0, void 0, function* () {
        try {
            const allergy = yield (0, exports.getOne)(id);
            if (!allergy) {
                return reject({ msg: "no such data exists." });
            }
            const queriedComments = yield client_1.default.allergies.findFirst({
                where: {
                    id: allergy.id,
                },
                select: {
                    comments: true,
                },
            });
            resolve(queriedComments);
        }
        catch (e) {
            reject((0, handleError_1.handleError)(e));
        }
    }));
};
exports.getComments = getComments;
